from sqlalchemy import Column,Integer,String,create_engine,Float
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from time import sleep
import requests as r
import string
import json


engine = create_engine("sqlite+pysqlite:///main.db",echo=False)

Session = sessionmaker(bind=engine)
session = Session()


Base=declarative_base()

class Coins(Base):
    __tablename__='crypto'
    id=Column(Integer,primary_key=True)
    ids=Column(String(50))
    curriences=Column(String(50))
    price_change=Column(Integer)
    price_change_in_1h=Column(Float)
    price_change_in_24h=Column(Float)
    price_change_in_7d=Column(Float)

    def present_price(self):
        coins_data=r.get('https://api.coingecko.com/api/v3/coins',params={'id':self.ids}).json()
        self.price_change=coins_data[0]['market_data']['current_price'][self.curriences]
        self.price_change_in_1h=coins_data[0]['market_data']["price_change_percentage_1h_in_currency"][self.curriences]
        self.price_change_in_24h=coins_data[0]['market_data']["price_change_24h_in_currency"][self.curriences]
        self.price_change_in_7d=coins_data[0]['market_data']["price_change_percentage_7d_in_currency"][self.curriences]


#Base.metadata.drop_all(engine)
#Base.metadata.create_all(engine)













