from locale import currency
from textwrap import indent
from time import sleep
import requests as r
import sqlalchemy as s
import json
from data_base import Coins,session

def continousloop(ids,currency):
    try:
        while(True):
            obj=Coins(ids=ids,curriences=currency)
            obj.present_price()
            session.add(obj)
            session.commit()
            sleep(120)
    except KeyboardInterrupt:
        print('program halted')    
continousloop('bitcoin','inr')